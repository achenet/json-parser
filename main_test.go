package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestParseObject(t *testing.T) {
	for input, expect := range map[string]map[string]interface{}{
		`{}`:              map[string]interface{}{},
		`{"key":"value"}`: map[string]interface{}{"key": "value"},
		`{"object":{"key":"value"}}`: map[string]interface{}{"object": map[string]interface{}{
			"key": "value",
		}},
		`{"key": true}`:  map[string]interface{}{"key": true},
		`{"key": false}`: map[string]interface{}{"key": false},
		`{"key": NULL}`:  map[string]interface{}{"key": nil},
		`{"key": 7}`:     map[string]interface{}{"key": 7.0},
		`{"key": []}`:    map[string]interface{}{"key": []interface{}{}},
		// FIXME
		// `{"key": [1, 2, 3]}`: map[string]interface{}{"key": []interface{}{1, 2, 3}},
	} {
		got, err := ParseObject(input)
		if err != nil {
			fmt.Printf("unexpected error on input %s: %s\n", input, err.Error())
			t.Fail()
		}
		if !reflect.DeepEqual(got, expect) {
			fmt.Printf("got: %v, expect %v on input %s\n", got, expect, input)
			t.Fail()
		}
	}
}

func TestSplitKeyValuePair(t *testing.T) {
	for _, tc := range []struct {
		input string
		key   string
		value string
		err   error
	}{
		{
			input: `"this":"that"`,
			key:   `"this"`,
			value: `"that"`,
			err:   nil,
		},
		{
			input: `"this":{}`,
			key:   `"this"`,
			value: `{}`,
			err:   nil,
		},
	} {
		key, value, err := SplitKeyValuePair(tc.input)
		if key != tc.key || value != tc.value || err != tc.err {
			fmt.Printf("got: key %s value %s err %v\nexpect: key %s value %s err %v", key, value, err, tc.key, tc.value, tc.err)
			t.Fail()
		}
	}
}

func TestParseValue(t *testing.T) {
	for input, expect := range map[string]interface{}{} {
		got, err := ParseValue(input)
		if err != nil {
			fmt.Printf("unxpected error on input %s: %s", input, err.Error())
			t.Fail()
		}
		if got != expect {
			fmt.Printf("got %v, expect %v on input %s\n", got, expect, input)
			t.Fail()
		}
	}
}
