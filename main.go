package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("json-parser")
}

func ParseObject(in string) (out map[string]interface{}, err error) {
	in = strings.TrimSpace(in)
	out = map[string]interface{}{}
	if in[0] != '{' || in[len(in)-1] != '}' {
		return nil, fmt.Errorf("input should start with '{' and end with '}'")
	}
	in = in[1 : len(in)-1] // remove leading and trailing '{'
	keyValuePairs := strings.Split(in, ",")
	// TODO keyValuePairs := SplitIntoKeyValuePairs(in)
	for _, p := range keyValuePairs {
		if len(p) == 0 {
			continue
		}
		key, valueString, err := SplitKeyValuePair(p)
		if err != nil {
			return nil, err
		}
		key = key[1 : len(key)-1] // remove leading and trailing `"`
		value, err := ParseValue(valueString)
		if err != nil {
			return nil, err
		}
		out[key] = value
	}
	return out, nil
}

func SplitKeyValuePair(p string) (key, value string, err error) {
	for i, char := range p {
		if char == ':' {
			return strings.TrimSpace(p[:i]), strings.TrimSpace(p[i+1:]), nil
		}
	}
	return "", "", fmt.Errorf("could not split key value pair %s", p)
}

func ParseValue(v string) (interface{}, error) {
	v = strings.TrimSpace(v)
	if v[0] == '"' {
		return v[1 : len(v)-1], nil
	}
	if v[0] == '{' {
		obj, err := ParseObject(v)
		if err != nil {
			return nil, err
		}
		return obj, nil
	}
	if v == "true" {
		return true, nil
	}
	if v == "false" {
		return false, nil
	}
	if v == "NULL" {
		return nil, nil
	}
	if num, err := strconv.ParseFloat(v, 64); err == nil {
		return num, nil
	}
	// TODO parse arrays
	if v[0] == '[' {
		return ParseArray(v)
	}
	return nil, fmt.Errorf("could not parse value: %s", v)
}

func ParseArray(v string) ([]interface{}, error) {
	v = v[1 : len(v)-1] // remove leading a trailing square brackets
	if len(v) == 0 {
		return []interface{}{}, nil
	}
	values := strings.Split(v, ",")
	out := make([]interface{}, len(values))
	for i, valueString := range values {
		val, err := ParseValue(valueString)
		if err != nil {
			return out, err
		}
		out[i] = val

	}
	return out, nil
}

func SplitIntoKeyValuePairs(in string) []string {
	// TODO
	return nil
}
